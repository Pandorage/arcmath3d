﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TrajectoryCalculation : MonoBehaviour
{
    //Bow transform (for the direction angles)
    public Transform bow;
    public GameObject trajectoryObject;

    private GameObject[] _objects = new GameObject[30];

    private void Start()
    {
        //Instantiate multiple gameobject to show the trajectory
        for (int i = 0; i < _objects.Length; i++)
        {
            _objects[i] = Instantiate(trajectoryObject, transform.position, transform.rotation);
        }
    }

    //Update the trajectory gameobject
    public void DisplayTrajectory(float force)
    {
        var v0 = bow.forward * force;
        var alpha = -bow.eulerAngles.x * Mathf.Deg2Rad;
        var distance = v0.magnitude*Mathf.Cos(alpha);
        float t = 0;
        foreach (var point in _objects)
        {
            t += distance / _objects.Length;
            point.transform.position = bow.position + v0 * t + 0.5f * Physics.gravity * (t * t);
        }
    }
}
