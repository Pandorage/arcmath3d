﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
public class Arrow : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Destroy arrow when it hit the ground
        if (other.gameObject.CompareTag("ground") && transform.parent == null)
        {
            Destroy(gameObject);
        }
    }
}
