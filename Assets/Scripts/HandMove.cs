﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class HandMove : MonoBehaviour
{
    public float moveSpeed;
    public float rotateSpeed;
    [Header("Move")]
    public KeyCode right;
    public KeyCode left;
    public KeyCode forward;
    public KeyCode back;
    public KeyCode up;
    public KeyCode down;
    [Header("Rotate")]
    public KeyCode yawPlus;
    public KeyCode yawLess;
    public KeyCode rollPlus;
    public KeyCode rollLess;
    public KeyCode pitchPlus;
    public KeyCode pitchLess;
    
    //Move the gameobject based on key input
    void Update()
    {
        if (Input.GetKey(right))
        {
            transform.Translate(Vector3.right * (Time.deltaTime * moveSpeed), Space.World);
        }
        if (Input.GetKey(left))
        {
            transform.Translate(Vector3.left * (Time.deltaTime * moveSpeed), Space.World);
        }
        if (Input.GetKey(forward))
        {
            transform.Translate(Vector3.forward * (Time.deltaTime * moveSpeed), Space.World);
        }
        if (Input.GetKey(back))
        {
            transform.Translate(Vector3.back * (Time.deltaTime * moveSpeed), Space.World);
        }
        if (Input.GetKey(up))
        {
            transform.Translate(Vector3.up * (Time.deltaTime * moveSpeed), Space.World);
        }
        if (Input.GetKey(down))
        {
            transform.Translate(Vector3.down * (Time.deltaTime * moveSpeed), Space.World);
        }

        if (Input.GetKey(yawPlus))
        {
            transform.Rotate(Vector3.up,rotateSpeed, Space.World);
        }
        if (Input.GetKey(yawLess))
        {
            transform.Rotate(Vector3.up,-rotateSpeed, Space.World);
        }
        if (Input.GetKey(rollPlus))
        {
            transform.Rotate(Vector3.forward, rotateSpeed, Space.World);
        }
        if (Input.GetKey(rollLess))
        {
            transform.Rotate(Vector3.forward, -rotateSpeed, Space.World);
        }
        if (Input.GetKey(pitchPlus))
        {
            transform.Rotate(Vector3.right, rotateSpeed, Space.World);
        }
        if (Input.GetKey(pitchLess))
        {
            transform.Rotate(Vector3.right, -rotateSpeed, Space.World);
        }
    }
}
