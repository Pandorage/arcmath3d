﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour
{
    public Transform hand;
    public Transform arrowPoint;
    public GameObject arrow;
    private bool _handled;
    public StringMovement ropeMov;
    private float _distanceRope;
    public KeyCode handleKey;
    public float handleLimit;
    public float handleDistance;
    private bool _launchTimer = true;
    public float timer;
    private GameObject _arrowScene;
    public TrajectoryCalculation trajectory;
    public float forceMultiplier = 5;

    // Update is called once per frame
    void Update()
    {
        _distanceRope = ropeMov.distanceStringOrigin;
        float distance = Vector3.Distance(hand.position, arrowPoint.position);

        //If the hand is near the arrow point,display it by making it green
        if (distance <= handleDistance || _handled)
        {
            hand.GetComponent<Renderer>().material.color = Color.green;
        }
        else
        {
            hand.GetComponent<Renderer>().material.color = Color.white;
        }
        
        //If the head is near the arrowPoint when key is pressed, start pulling the arrowPoint
        if (Input.GetKeyDown(handleKey) && distance <= handleDistance && !_launchTimer)
        {
            _handled = true;
        }

        //If key is released, reset the arrowPoint and shot the arrow
        if (Input.GetKeyUp(handleKey) && _handled)
        {
            var rb = _arrowScene.GetComponent<Rigidbody>();
            rb.useGravity = true;
            rb.AddForce(transform.forward * _distanceRope * forceMultiplier,ForceMode.Impulse);
            _arrowScene.transform.parent = null;
            _handled = false;
            arrowPoint.localPosition = Vector3.zero;
            _launchTimer = true;
            timer = 3;
        }

        //Timer use to instantiate arrow
        if (_launchTimer)
        {
            if (timer >= 0)
            {
                timer -= Time.deltaTime;
            }

            if (timer <= 0)
            {
                _launchTimer = false;
               _arrowScene = Instantiate(arrow, arrowPoint);
            }
        }
        

        //Id the arrowPoint is pulled, change it position based on hand position
        if (_handled)
        {
            Vector3 pos = arrowPoint.localPosition;
            pos.z = transform.InverseTransformPoint(hand.position).z;
            if (pos.z <= 0 && pos.z > -handleLimit)
            {
                arrowPoint.localPosition = pos;
            }
            trajectory.DisplayTrajectory(_distanceRope * forceMultiplier);
        }
    }
}
