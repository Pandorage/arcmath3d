﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringMovement : MonoBehaviour
{
    public Transform stringUp;
    public Transform stringDown;
    public Transform arrowPoint;
    public float distanceStringOrigin;
    void Update()
    {
        //Change position of string to place them between the arrow point and the bow
        Vector3 pos = stringUp.localPosition;
        pos.z = arrowPoint.localPosition.z / 2;
        stringUp.localPosition = pos;
        pos = stringDown.localPosition;
        pos.z = arrowPoint.localPosition.z / 2;
        stringDown.localPosition = pos;
        
        //Change their rotation
        float stringLenght = Vector3.Distance(stringUp.localPosition, stringDown.localPosition);
        distanceStringOrigin = Vector3.Distance(Vector3.zero, arrowPoint.localPosition);
        float tang = Mathf.Atan2(distanceStringOrigin, stringLenght) * Mathf.Rad2Deg;
        stringUp.localRotation = Quaternion.Euler(-tang,0,0);
        stringDown.localRotation = Quaternion.Euler(tang,0,0);
        
        //Change their scale to adapt to the arrow point distance
        stringUp.localScale = new Vector3(0.1f, 0.3f+ 0.3f * -arrowPoint.localPosition.z, 0.1f);
        stringDown.localScale = new Vector3(0.1f, 0.3f+ 0.3f * -arrowPoint.localPosition.z, 0.1f);
    }
}
